package suchatpong.nipitphon.lab4;
/**
 * write the code as determines
 * create isFaster to check car1 or car2 faster
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 5, 2019
 */
public class TestDrive2 {

	public static void main(String[] args) {

		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");

		System.out.println(car1);
		System.out.println(car2);

		car1.accelerate();
		car2.accelerate();
		car2.accelerate();

		System.out.println(car1);
		System.out.println(car2);

		car1.brake();
		car1.brake();
		car2.brake();

		System.out.println(car1);
		System.out.println(car2);

		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
	}

	public static void isFaster(Automobile car1, Automobile car2) {
		int car1_speed = car1.getSpeed();
		int car2_speed = car2.getSpeed();
		if (car1_speed > car2_speed) {
			System.out.println(car1.getModel() + " is faster then " + car2.getModel());
			System.out.println(car2.getModel() + " is NOT faster then " + car1.getModel());
		} else if (car2_speed > car1_speed) {
			System.out.println(car1.getModel() + " is NOT faster then " + car2.getModel());
			System.out.println(car2.getModel() + " is faster then " + car1.getModel());
		} else {
			System.out.println(car1.getModel() + " is NOT faster then " + car2.getModel());
			System.out.println(car2.getModel() + " is NOT faster then " + car1.getModel());
		}
	}

}
