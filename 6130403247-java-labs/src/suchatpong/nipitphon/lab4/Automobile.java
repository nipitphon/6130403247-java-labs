package suchatpong.nipitphon.lab4;
/**
 * Announcement of various variable types
 * create enum of color 
 * create Automobile to run program of Automobile
 * create set and get various variable types
 * create String toString() to out when Automobile works
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 5, 2019
 */
public class Automobile {
	private int gasoline;
	private int speed;
	private int MaxSpeed;
	private int acceleration;
	private String model;
	private Color color;
	private static int numberOfAutomobile = 0;

	enum Color {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET, WHITE, BLACK
	}

	public Automobile(int gasoline, int speed, int MaxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.MaxSpeed = MaxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile++;
	}

	public Automobile() {
		this.gasoline = 0;
		this.speed = 0;
		this.MaxSpeed = 0;
		this.acceleration = 160;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile++;
	}

	public int getGasoline() {
		return gasoline;
	}

	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getMaxSpeed() {
		return MaxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		MaxSpeed = maxSpeed;
	}

	public int getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}

	public static void setNumberOfAutomobile(int numberOfAutomobile) {
		Automobile.numberOfAutomobile = numberOfAutomobile;
	}

	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", MaxSpeed=" + MaxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}

}
