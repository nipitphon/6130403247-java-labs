package suchatpong.nipitphon.lab4;
/**
 * ToyotaAuto get information from Automobile 
 * create ToyotaAuto to run program of ToyotaAuto
 * create refuel to change Gasoline to 100
 * create accelerate to increase speed and decreases gasoline by 15
 * create brake to decreases speed and decreases gasoline by 15
 * create setspeed to check speed if speed < 0 change to 0 and if speed > maxspeed change to maxspeed
 * create String toString() to out when ToyotaAuto works
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 5, 2019
 */
public class ToyotaAuto extends Automobile implements Movable, Refuelable {

	public ToyotaAuto(int maximum_speed, int acceleration, String model) {
		setGasoline(100);
		setMaxSpeed(maximum_speed);
		setAcceleration(acceleration);
		setModel(model);
	}

	public void refuel() {
		setGasoline(100);
		System.out.println(getModel() + " refuels");
	}

	public void accelerate() {
		int old_speed = getSpeed();
		int acceleration = getAcceleration();
		int maxspeed = getMaxSpeed();
		int gasoline = getGasoline();
		if (old_speed + acceleration < maxspeed) {
			setSpeed(old_speed + acceleration);
			setGasoline(gasoline - 15);
			System.out.println(getModel() + " accelerates");
		} else {
			setSpeed(maxspeed);
			setGasoline(gasoline - 15);
			System.out.println(getModel() + " accelerates");
		}
	}

	public void brake() {
		int old_speed = getSpeed();
		int acceleration = getAcceleration();
		int gasoline = getGasoline();
		if (old_speed - acceleration < 0) {
			setSpeed(0);
			setGasoline(gasoline - 15);
			System.out.println(getModel() + " brake");
		} else {
			setSpeed(old_speed - acceleration);
			setGasoline(gasoline - 15);
			System.out.println(getModel() + " brake");
		}
	}

	public void setspeed(int check_speed) {
		check_speed = getSpeed();
		int maxspeed = getMaxSpeed();
		if (check_speed < 0) {
			setSpeed(0);
		} else if (check_speed > maxspeed) {
			setSpeed(maxspeed);
		}

	}

	public String toString() {
		return getModel() + " gas : " + getGasoline() + " speed : " + getSpeed() + " max speed : " + getMaxSpeed()
				+ " acceleration : " + getAcceleration();
	}

}
