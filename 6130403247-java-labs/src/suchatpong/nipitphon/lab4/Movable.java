package suchatpong.nipitphon.lab4;
/**
 * create interface Movable to preserve accelerate and brake
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 5, 2019
 */
public interface Movable {
	public void accelerate();

	public void brake();

	public void setspeed(int check_speed);
}
