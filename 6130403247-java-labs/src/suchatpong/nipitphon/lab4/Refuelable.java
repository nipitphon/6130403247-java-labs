package suchatpong.nipitphon.lab4;
/**
 * create interface Movable to preserve refuel
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 5, 2019
 */
public interface Refuelable {
	public void refuel();

}
