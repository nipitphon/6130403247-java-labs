package suchatpong.nipitphon.lab5;
/**
 * create variables modelName , brandName and price
 * create AndroidSmartWatch to use variables in MobileDevice (use 3 variables)
 * create getter and setter variables
 * create toString
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 19, 2019
 */
public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;
	
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
	}
	
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.brandName = brandName;
		this.modelName = modelName;
		this.price = price;
	}
	
	public void displayTime()	{
		System.out.println("Display time only using a digital format");
	}
	
	public String toString() {
		return "AndroidSmartWatch [BrandName: " + brandName + " Model Name: " + modelName + " Price: " + price + "bath]";
	}
}
