package suchatpong.nipitphon.lab5;

public class InterestedMobileDevices {

	public static void main(String[] args) {
		MobileDevice galaxyNote9 = new MobileDevice("galaxyNote 9", "Android", 25500, 201);
		MobileDevice ipadGen6 = new MobileDevice("Apple ipad Mini 3", "ios", 11500);
		System.out.println(galaxyNote9);
		System.out.println(ipadGen6);
		ipadGen6.setPrice(11000);
		System.out.println(ipadGen6.getModelName() + " has new price as " + ipadGen6.getPrice() + " baht.");
		System.out.println(galaxyNote9.getModelName() + " has " + "weight as " + galaxyNote9.getWeight() + " grams.");	
	}
}
