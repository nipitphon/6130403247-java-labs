package suchatpong.nipitphon.lab5;
/**
 * create variables brand , androidVersion and set brand to "Samsung"
 * create SamsungDevice to use variables in MobileDevice (use 3 variables)
 * create SamsungDevice to use variables in MobileDevice (use 4 variables)
 * create toString
 * create getter and setter variables
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 19, 2019
 */
public class SamsungDevice extends MobileDevice {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String brand  = "Samsung";
	private double androidVersion;

	public static String getBrand() {
		return brand;
	}

	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}

	public SamsungDevice(String modelName, int price, double androidVersion) {
		super(modelName, "Android", price);
		this.androidVersion = androidVersion;
	}

	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super(modelName, "Android", price, weight);
		this.androidVersion = androidVersion;
	}
	
	public String toString() {
		return "MobileDevice ["+ super.output() + " AndroidVersion: " + androidVersion + "]";
	}

	public void displayTime()	{
		System.out.println("Display times in both using a digital format and using an analog watch");
	}
	
}
