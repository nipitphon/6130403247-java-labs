package suchatpong.nipitphon.lab5;
/**
 * create abstract usage to use in another class
 * create method getos
 * create constant os to Android
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 19, 2019
 */
public abstract class AndroidDevice {
	
	private static final String os = "Android" ;
	
	abstract void usage();
	
	public String getos() {
		return os ;
	}
}
