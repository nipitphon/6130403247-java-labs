package suchatpong.nipitphon.lab5;

import java.io.Serializable;

/**
 * create variables modelName, os, price and weight
 * create MobileDevice for receive variables 3 and 4 variables to work in method
 * create getter and setter variables
 * create toString
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 19, 2019
 */
public class MobileDevice implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String modelName;
	private String os;
	private int price;
	private int weight;
	
	public MobileDevice(String modelName, String os, int price, int weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}

	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
	}
	
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public String output() {
		return "model name:" + modelName + ", OS: " + os + ", Price:" + price + " bath, weight:" + weight + " g";
	}
	
	public String toString() {
		return "MobileDevice ["+ output() +"]";
	}
	
}
