package suchatpong.nipitphon.lab3;

import java.util.Random;
import java.util.Scanner;

/**
 * When input (exit) scanner has close and system exit
 * when input is not head and tail program has run again 
 * com_random = 0 to head and com_random = 1 to tail 
 * if input equals com_random you win but
 * not equals com_random computer win
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 4, 2019
 */
public class MatchingPenny {
	
	public static void main(String[] args) {
		Random random = new Random();
		Scanner scanner = new Scanner(System.in);
		boolean play = true;	
		while (play) {
			System.out.print("Enter head or tail : ");
			String input = scanner.nextLine();
			String you_play = input.toLowerCase();
			if (you_play.equals("exit")) { 
				System.out.println("Good bye");
				scanner.close();
				System.exit(0); 
			} 
			else if (you_play.equals("head") || you_play.equals("tail")) { 
				System.out.println("You play " + you_play);
				int com_random = random.nextInt(2);
				if (com_random == 0) { 
					String complay = "head";
					System.out.println("Computer plays head");

					if (you_play.equals(complay)) { 
						System.out.println("You win");
					} else {
						System.out.println("Computpter win");
					}
				} else {
					String complay = "tail";
					System.out.println("Computer plays tail");
					if (you_play.equals(complay)) {
						System.out.println("You win");
					} else {
						System.out.println("Computpter win");
					}
				}
			} 
			else {
				System.err.print("Incorrect input. head or tail only\n");
			}
		}
	}
}

