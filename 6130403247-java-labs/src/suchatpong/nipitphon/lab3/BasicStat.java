package suchatpong.nipitphon.lab3;

import java.util.Arrays;

/**
 * args[0] = to numbers of input 
 * args[1-...] to arrays and sort arrays
 * last number in arrays is min
 * first number in arrays is max
 * , run Average and standard deviation
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 4, 2019
 */
public class BasicStat {

	static double num[];
	static int numInput;

	public static void acceptInput(String[] args) {
		numInput = Integer.parseInt(args[0]);
		if ((args.length - 1) != numInput) {
			System.err.println("<BasicStat><numNumbers><numbers>...");
			System.exit(0);
		}
	}

	public static void displayStats(String[] args) {
		double[] num = new double[args.length -1];
		for (int i = 1 ; i < args.length ; i ++) {
			 double a = Double.parseDouble(args[i]);
			 num[i-1] = a;
		}
		Arrays.sort(num);
		double total = 0;
		for (int i = 0 ; i < num.length ; i++) {
			total += num[i] ;
		}
		double total_sta = 0;
		for (int i = 0 ; i < num.length ; i++) {
			total_sta += Math.pow(num[i]-(total/num.length),2);
		}
		double standard_num = Math.sqrt(total_sta/num.length);
		System.out.println("Max is " + num[0] + " Min is " + num[num.length -1]);
		System.out.println("Average is " + total/num.length);
		System.out.println("standard deviation is " + standard_num);
		
	} 

	public static void main(String[] args) {
		acceptInput(args);
		displayStats(args);
	}
}
