package suchatpong.nipitphon.lab3;

import java.util.Random;
import java.util.Scanner;
/**
 * String color()
 * 	color_random use switch case to random color 7 color and put to passage
 * input to upper if input equals passage scanner has close while has true
 * time = (end - start)/1000 to second 
 * 
 * 
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 4, 2019
 */
public class TypingTest {

	public static String color() {
		Random random = new Random();
		int color_random = random.nextInt(6);
		String color = null;
	    switch (color_random) {
	        case 0:  color = "RED";
	                 break;
	        case 1:  color = "ORANGE";
	                 break;
	        case 2:  color = "YELLOW";
	                 break;
	        case 3:  color = "GREEN";
	                 break;
	        case 4:  color = "BLUE";
	                 break;
	        case 5:  color = "INDIGO";
	                 break;
	        case 6:  color = "VIOLET";
	                 break;
	    }
	    return color;
	}
	
	public static void main(String[] args) {
		double start = System.currentTimeMillis();
		String passage = color() + " " + color() + " " + color() + " " + color() + " " + 
						 color() + " " + color() + " " + color() ;
		System.out.println(passage);
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.print("Type your answer : ");
			String input = scanner.nextLine();
			if (input.toUpperCase().equals(passage)) {
				scanner.close();
				break ;	
			}
		}
		double end = System.currentTimeMillis();
		double time = (end - start)/1000 ;
		System.out.println("Your time is " + time);
		if (time >= 12) {
			System.out.println("You type slower than average person");
		}
		else {
			System.out.println("You type faster than average person");
			}
		}
	}
