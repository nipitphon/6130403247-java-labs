package suchatpong.nipitphon.lab3;

import java.util.Random;
import java.util.Scanner;
/**
 *acceptInput()
 *	when you_play = head or tail scanner close and loop exit but you_play = exit to system exit
 *String genComChoice()
 *	com_random = 0 to head and com_random = 1 to tail
 *displayWiner(String you, String com)
 *	if you equals com you has win but not equals com has win
 *
 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: February 4, 2019
 */
public class MatchingPennyMethod {

	public static String acceptInput() {
		Scanner scanner = new Scanner(System.in);
		boolean play = true;
		String you_play = null ;
		while (play) {
			System.out.print("Enter head or tail : ");
			String input = scanner.nextLine();
			you_play = input.toLowerCase();
			if (you_play.equals("head") || you_play.equals("tail")) {
				scanner.close();
				play = false;
			} else if (you_play.equals("exit")) {
				System.out.println("Good bye");
				scanner.close();
				System.exit(0);
			} else {
				System.err.println("Incorrect input. head or tail only");
			}
		}
		return you_play;
	}

	public static String genComChoice() {
		Random random = new Random();
		int com_random = random.nextInt(2);
		String num = null;
		switch (com_random) {
		case 0: num = "head";
			break;
		case 1: num = "tail";
			break;
		}
		return num;
	}

	public static void displayWiner(String you, String com) {
		if (you.equals(com)) {
			System.out.println("you win");
		} else {
			System.out.println("Computpter win");
		}
	}

	public static void main(String[] args) {
			System.out.println("You play " + acceptInput());
			System.out.println("Computer plays " + genComChoice());
			displayWiner(acceptInput(), genComChoice());
	}
}
