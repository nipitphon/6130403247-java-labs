package suchatpong.nipitphon.lab7;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {

	/**
	 * create ImagePanel 
	 * create image_panal to add image
	 * add image_panal in total_panal
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 4, 2019
	 */
	private static final long serialVersionUID = 1L;

	protected JPanel  panal_southV2;
	protected ImagePanel image_panal ;
	
	
	public MobileDeviceFormV6(String Topic) {
		super(Topic);
	}
	
	protected void addComponents() {
		super.addComponents();
		total_panal.remove(panal_south);
		
		image_panal = new ImagePanel("Images/galaxyNote9.jpg");
		
		panal_southV2 = new JPanel();
		panal_southV2.setLayout(new BorderLayout());
		panal_southV2.add(image_panal, BorderLayout.NORTH);
		panal_southV2.add(panal_south, BorderLayout.SOUTH);
		
		total_panal.add(panal_southV2, BorderLayout.SOUTH);
	}
	  
	
	public static void createAndShowGUI() {
		MobileDeviceFormV6 MobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");
		MobileDeviceFormV6.addComponents();
		MobileDeviceFormV6.addMenus();
		MobileDeviceFormV6.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	
}
