package suchatpong.nipitphon.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import suchatpong.nipitphon.lab6.MobileDeviceFormV3;

public class MobileDeviceFormV4 extends MobileDeviceFormV3{

	/**
	 * in updateMenuIcon remove filemenu to change menuitem_new (input ImageIcon)
	 * addSubMenus remove configmenu to change menus all of configmenu
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 4, 2019
	 */
	private static final long serialVersionUID = 1L;

	protected JMenu menu_color ,menu_size ;
	protected JMenuItem menuitem_red ,menuitem_green ,menuitem_Blue ,menuitem_size16 ,menuitem_size20 ,menuitem_size24;
	
	public MobileDeviceFormV4(String Topic) {
		super(Topic);
	}

	protected void updateMenuIcon() {
		filemenu.removeAll();
		menuitem_new = new JMenuItem("New", new ImageIcon("Images/new.jpg"));
		filemenu.add(menuitem_new);
		filemenu.add(menuitem_open);
		filemenu.add(menuitem_save);
		filemenu.add(menuitem_exit);
	}

	protected void addSubMenus() {
		configmenu.removeAll();
		menu_color = new JMenu("Color");
		menuitem_red = new JMenuItem("red");
		menuitem_green = new JMenuItem("Green");
		menuitem_Blue = new JMenuItem("Blue");
		menu_color.add(menuitem_red);
		menu_color.add(menuitem_green);
		menu_color.add(menuitem_Blue);
		configmenu.add(menu_color);
		
		menu_size = new JMenu("Size");
		menuitem_size16  = new JMenuItem("16");
		menuitem_size20 = new JMenuItem("20");
		menuitem_size24 = new JMenuItem("24");
		menu_size.add(menuitem_size16);
		menu_size.add(menuitem_size20);
		menu_size.add(menuitem_size24);
		configmenu.add(menu_size);
	}
	
	protected void addMenus(){
		super.addMenus();
		updateMenuIcon();
		addSubMenus();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV4 MobileDeviceFormV4 = new MobileDeviceFormV4("Mobile Device Form V4");
		MobileDeviceFormV4.addComponents();
		MobileDeviceFormV4.addMenus();
		MobileDeviceFormV4.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
