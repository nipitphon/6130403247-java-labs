package suchatpong.nipitphon.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingUtilities;

public class MobileDeviceFormV5 extends MobileDeviceFormV4{

	/**
	 * in initComponents change Font label and text 
	 * 					 change cancel_button to red 
	 * 					 change ok_button to blue
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 4, 2019
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV5(String Topic) {
		super(Topic);
	}

	protected void initComponents() {
		label_Brand.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Model.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Weight.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Price.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Mobile.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Type.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Review.setFont(new Font("Serif", Font.PLAIN, 14));
		label_Features.setFont(new Font("Serif", Font.PLAIN, 14));
		text_Brand.setFont(new Font("Serif", Font.BOLD, 14));
		text_Model.setFont(new Font("Serif", Font.BOLD, 14));
		text_Weight.setFont(new Font("Serif", Font.BOLD, 14));
		text_Price.setFont(new Font("Serif", Font.BOLD, 14));
		cancel_button.setForeground(Color.RED);
		ok_button.setForeground(Color.BLUE);
	}
	
	protected void addComponents() {
		super.addComponents();
		initComponents();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV5 MobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
		MobileDeviceFormV5.addComponents();
		MobileDeviceFormV5.addMenus();
		MobileDeviceFormV5.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
