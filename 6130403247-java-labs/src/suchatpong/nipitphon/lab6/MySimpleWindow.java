package suchatpong.nipitphon.lab6;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

class MySimpleWindow extends JFrame {
	
	/**
	 * method addComponents ---> create panel to add ok_button , cancel_button
	 * method createAndShowGUI ---> set location to In the middle of the screen
	 * 							    set exit frame when click close
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 1, 2019
	 */
	private static final long serialVersionUID = 1L;
	
	protected JPanel panal ;
	protected JButton cancel_button ,ok_button ;
	
	public MySimpleWindow(String Topic) {
		super(Topic);
	}

	protected void  addComponents () {
		panal = new JPanel();
		cancel_button = new JButton("cancel");
		ok_button = new JButton("ok");
		panal.add(cancel_button);
		panal.add(ok_button);
		this.add(panal);
		this.pack();
	}
	
	protected void setFrameFeatures() {
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				}
		});
	}
}
