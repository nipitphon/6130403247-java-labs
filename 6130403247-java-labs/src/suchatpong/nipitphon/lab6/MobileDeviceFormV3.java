package suchatpong.nipitphon.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {

	/**
	 * create panal_features is BorderLayout and create JList input in panal_features - EAST
	 * add panal_features in panal_center - CENTER 
	 * create method addMenus 
	 * create JMenuBar and input new JMenu(File) in JMenuBar
	 * input 4 JMenuItem in JMenu(File)
	 * input new JMenu(Config) in JMenuBar
	 * input 2 JMenuItem in JMenu(Config)
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 1, 2019
	 */
	private static final long serialVersionUID = 1L;

	protected JPanel panal_features , panal_centerV2;
	protected JLabel label_Features ;
	protected JMenuBar menubar ;
	protected JMenu filemenu, configmenu ;
	protected JMenuItem menuitem_new, menuitem_open, menuitem_save, menuitem_exit, menuitem_color, menuitem_size ;
	protected JList<String> jlist;
	
	public MobileDeviceFormV3(String Topic) {
		super(Topic);
	}

	protected void addComponents() {
		super.addComponents();

		panal_features = new JPanel();
		panal_features.setLayout(new GridLayout(0,2));
		label_Features = new JLabel("Features : ");
		panal_features.add(label_Features);
		String list[] = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
		jlist = new JList<String>(list);
		panal_features.add(jlist);

		panal_centerV2 = new JPanel();
		panal_centerV2.setLayout(new BorderLayout());
		panal_centerV2.add(panal_features, BorderLayout.NORTH);
		panal_centerV2.add(panal_center, BorderLayout.SOUTH);
		
		total_panal.add(panal_centerV2, BorderLayout.CENTER);

		this.add(total_panal);

	}

	protected void addMenus() {
		menubar = new JMenuBar();
		filemenu = new JMenu("File");
		menubar.add(filemenu);

		menuitem_new = new JMenuItem("New");
		filemenu.add(menuitem_new);
		menuitem_open = new JMenuItem("open");
		filemenu.add(menuitem_open);
		menuitem_save = new JMenuItem("save");
		filemenu.add(menuitem_save);
		menuitem_exit = new JMenuItem("exit");
		filemenu.add(menuitem_exit);

		configmenu = new JMenu("Config");
		menubar.add(configmenu);

		menuitem_color = new JMenuItem("Color");
		configmenu.add(menuitem_color);
		menuitem_size = new JMenuItem("Size");
		configmenu.add(menuitem_size);

		this.setJMenuBar(menubar);
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV3 MobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		MobileDeviceFormV3.addComponents();
		MobileDeviceFormV3.addMenus();
		MobileDeviceFormV3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
