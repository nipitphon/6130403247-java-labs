package suchatpong.nipitphon.lab6;

import java.awt.BorderLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {

	/**
	 * in panal_center_north is BorderLayout and in panal_center_north - EAST is JComboBox
	 * in panal_center_south is BorderLayout and in panal_center_south - EAST is JTextArea
	 * add panal_center main panel 
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 1, 2019
	 */
	private static final long serialVersionUID = 1L;

	protected JPanel panal_center ;
	protected JLabel label_Type, label_Review;
	protected JComboBox<String> Type;
	protected JTextArea Review;
	
	public MobileDeviceFormV2(String Topic) {
		super(Topic);
	}

	protected void  addComponents () {
		super.addComponents();

		label_Type = new JLabel("Type : ");
		panal_north.add(label_Type);
		Type = new JComboBox<String>();
		Type.addItem("Phone");
		Type.addItem("Tablet");
		Type.addItem("Smart TV");
		panal_north.add(Type);
		
		panal_center = new JPanel();
		panal_center.setLayout(new BorderLayout());
		label_Review = new JLabel("Review : ");
		panal_center.add(label_Review, BorderLayout.NORTH);
		Review = new JTextArea(3,35);
		Review.setLineWrap(true);
		Review.setWrapStyleWord(true);
		Review.setText("Bigger than previous Note phones in every way,"
				+ " the Samsung Galaxy Note 9 has a larger 6.4-inch screen,"
				+ " heftier 4,000mAh battery,"
				+ " and a massive 1TB of storage option. ");
		JScrollPane sp = new JScrollPane(Review);
		panal_center.add(sp, BorderLayout.SOUTH);

		total_panal.add(panal_center, BorderLayout.CENTER);
		
		this.add(total_panal);
		this.pack();
	}
	
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV2 MobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		MobileDeviceFormV2.addComponents();
		MobileDeviceFormV2.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
