package suchatpong.nipitphon.lab6;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.*;

class MobileDeviceFormV1 extends MySimpleWindow  {

	/**
	 * add GridLayout in BorderLayout - north and in 5-2 create ButtonGroup(Android , ios)
	 * create BorderLayout-south input panel (ok_button , cancel_button)
	 * 
	 * @author Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: March 1, 2019
	 */
	private static final long serialVersionUID = 1L;

	protected JPanel panal_north, panal_south, total_panal, panal_os ;
	protected JLabel label_Brand, label_Model, label_Weight, label_Price, label_Mobile ;
	protected JTextField text_Brand, text_Model, text_Weight, text_Price; 
	protected JRadioButton Android, ios;
	protected ButtonGroup button;
	
	public MobileDeviceFormV1(String Topic) {
		super(Topic);
	}
	
	protected void  addComponents () {
		super.addComponents();
		
		panal_north = new JPanel();
		
		panal_north.setLayout(new GridLayout(0,2));
		
		label_Brand = new JLabel("Brand Name : ");
		panal_north.add(label_Brand);
		text_Brand = new JTextField(15);
		panal_north.add(text_Brand);
		
		label_Model = new JLabel("Model Name : ");
		panal_north.add(label_Model);
		text_Model = new JTextField(15);
		panal_north.add(text_Model);
		
		label_Weight = new JLabel("Weight (kg.) : ");
		panal_north.add(label_Weight);
		text_Weight = new JTextField(15);
		panal_north.add(text_Weight);
		
		label_Price = new JLabel("Price (Baht) : ");
		panal_north.add(label_Price);
		text_Price = new JTextField(15);
		panal_north.add(text_Price);
		
		label_Mobile = new JLabel("Mobile OS : ");
		panal_north.add(label_Mobile);
		Android = new JRadioButton("Android");
		ios = new JRadioButton("ios");
		button = new ButtonGroup();
		button.add(Android);
		button.add(ios);
		panal_os = new JPanel();
		panal_os.setLayout(new GridLayout(0,2));
		panal_os.add(Android);
		panal_os.add(ios);
		panal_north.add(panal_os);	
		
		panal_south = new JPanel();
		panal_south.add(panal);
		
		total_panal = new JPanel();
		total_panal.setLayout(new BorderLayout());
		total_panal.add(panal_north, BorderLayout.NORTH);
		total_panal.add(panal_south, BorderLayout.SOUTH);
		
		this.add(total_panal);
		this.pack();
	}
	
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}
	
	public static void main(String[] args) {
		MobileDeviceFormV1 MobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		MobileDeviceFormV1.addComponents();
		MobileDeviceFormV1.setFrameFeatures();
	}
	
	public static void createAndShowGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
				}
		});
	}
}
