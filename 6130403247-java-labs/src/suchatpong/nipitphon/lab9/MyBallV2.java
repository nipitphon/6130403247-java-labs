package suchatpong.nipitphon.lab9;

import suchatpong.nipitphon.lab8.MyBall;

public class MyBallV2 extends MyBall {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	int ballVelX , ballVelY;
	
	public MyBallV2(int x, int y) {
		super(x, y);
	}

	public void move() {
		super.x += ballVelX;
		super.y += ballVelY;
		
	}
	
}
