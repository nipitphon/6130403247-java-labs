package suchatpong.nipitphon.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import suchatpong.nipitphon.lab8.MyBall;
import suchatpong.nipitphon.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected MyBallV2 ball = new MyBallV2(0, (HEIGHT - MyBall.diameter)/2);
	protected Thread running = new Thread(this);
	
	public MyCanvasV5() {
		ball.ballVelX = 2 ;
		ball.ballVelY = 0 ;
		running.start();
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		//Draw a ball 
		g2d.setColor(Color.white);
		g2d.fill(ball);
	}
	
	public void run() {
		while (true) {
			
			// Stop the ball when hit the right side of the wall
			if (ball.x + MyBall.diameter >= super.WIDTH) {
				ball.ballVelX = 0;
			}
			
			// move the ball
			ball.move();
			repaint();
			
			//Delay
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
		
	}

}
