package suchatpong.nipitphon.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import suchatpong.nipitphon.lab8.MyBall;
import suchatpong.nipitphon.lab8.MyBrick;

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int numBricks = WIDTH / MyBrick.brickWidth;
	protected MyBrickV2[] arraybricks = new MyBrickV2[numBricks];
	Thread running = new Thread(this);
	MyBallV2 ball3;

	public MyCanvasV7() {
		ball3 = new MyBallV2(0, 0);
		ball3.ballVelX = 2;
		ball3.ballVelY = 2;
		for (int i = 0; i < numBricks; i++) {
			arraybricks[i] = new MyBrickV2(MyBrick.brickWidth * i, HEIGHT / 2);
		}
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		g2d.setColor(Color.white);
		g2d.fill(ball3);
		
		for (int i = 0; i < numBricks; i++) {
			if (arraybricks[i].visible) {
				g2d.setColor(Color.red);
				g2d.fill(arraybricks[i]);
				g2d.setColor(Color.blue);
				g2d.draw(arraybricks[i]);
			}
		}
	}

	@Override
	public void run() {
		while (true) {
			// bounce the ball when hit the right or the left side of the wall
			if (ball3.x + MyBall.diameter >= super.WIDTH || ball3.x <= 0) {
				ball3.ballVelX *= -1;
			}

			// bouncing the ball when it hits the top and bottom of the wall
			if (ball3.y + MyBall.diameter >= super.HEIGHT || ball3.y <= 0) {
				ball3.ballVelY *= -1;
			}

			for (int i = 0; i < numBricks; i++) {
				if (arraybricks[i].visible) {
					checkCollision(ball3, arraybricks[i]);
				}
			}

			// move the ball
			ball3.move();
			repaint();

			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {

			}
		}
	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter/2;
		double y = ball.y + MyBall.diameter/2;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrickV2.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrickV2.brickHeight));
		
		boolean collided = (deltaX*deltaX + deltaY*deltaY) < (MyBall.diameter*MyBall.diameter)/4;
		
		if(collided) {
			if (deltaX*deltaX < deltaY*deltaY) {
				//collides top or bottom
				//Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;
			}
			else {
				//collides left or right
				//Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false ;
		}
	}
}
