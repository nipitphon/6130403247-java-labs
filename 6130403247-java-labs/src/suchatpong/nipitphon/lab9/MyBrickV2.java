package suchatpong.nipitphon.lab9;

import suchatpong.nipitphon.lab8.MyBrick;

public class MyBrickV2 extends MyBrick{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected boolean visible = true ;
	
	public MyBrickV2(int x, int y) {
		super(x, y);
	}

}
