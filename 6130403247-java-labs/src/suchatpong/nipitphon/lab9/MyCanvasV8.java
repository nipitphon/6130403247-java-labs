package suchatpong.nipitphon.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import suchatpong.nipitphon.lab8.MyBall;
import suchatpong.nipitphon.lab8.MyBrick;
import suchatpong.nipitphon.lab8.MyCanvas;
import suchatpong.nipitphon.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int numCol = WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks = new MyBrickV2[numRow][numCol];
	protected Thread running;
	protected Color[] color = { Color.magenta, Color.blue, Color.cyan, Color.green, Color.yellow, Color.orange,
			Color.red };
	protected MyPedalV2 pedal;
	protected int lives;
	protected MyBallV2 ball4;
	Random random = new Random();

	public MyCanvasV8() {
		// initialize bricks with numRow and numCol

		// initialize ball with
		// MyCanvas.WIDTH/2 - Myball.diameter/2 and
		// MyCanvas.HEIGHT - Myball.diameter - MyPedal.pedalHeight
		ball4 = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2,
				MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball4.ballVelX = 0;
		ball4.ballVelY = 0;

		running = new Thread(this);

		// initialize bricks with j*MyBrick.brickWidth for x
		// and super.HEIGHT/3 - i*MyBrick.brickHeight for y
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, HEIGHT / 3 - i * MyBrick.brickHeight);
			}
		}

		setFocusable(true);
		addKeyListener(this);

		// initialize pedal with MyCanvas.WIDTH/2 - MyPedal.pedalWidth/2
		// and MyCanvas.HEIGHT - MyPedal.pedalHeight
		pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);

		lives = 3;
		
		running.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.black);
					g2d.draw(bricks[i][j]);
				}
			}
		}

		// Draw a ball
		g2d.setColor(Color.white);
		g2d.fill(ball4);

		// Draw a pedal
		g2d.setColor(Color.gray);
		g2d.fill(pedal);

		// Draw score
		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.blue);
		String s = "Lives : " + lives;
		g2d.drawString(s, 10, 30);

		// Draw a String You won
		if (numVisibleBricks == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
			g2d.setColor(Color.green);
			String won = "You won";
			g2d.drawString(won, MyCanvas.WIDTH/2 - 150, MyCanvas.HEIGHT/2);
		}

		// Draw a string game over
		if (lives == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 50));
			g2d.setColor(Color.gray);
			String over = "GAME OVER";
			g2d.drawString(over, MyCanvas.WIDTH/2 - 150, MyCanvas.HEIGHT/2);
		}
	}

	private void checkPassBottom() {
		if (ball4.y >= MyCanvas.HEIGHT) {
			ball4.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
			ball4.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball4.ballVelX = 0;
			ball4.ballVelY = 0;
			lives--;
			repaint();
		}
	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2;
		double y = ball.y + MyBall.diameter / 2;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				// collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;
			} else {
				// collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;
			}
			numVisibleBricks--;
			ball.move();
			brick.visible = false;
		}
	}

	public void collideWithpedal() {
		double x = ball4.x + MyBall.diameter / 2;
		double y = ball4.y + MyBall.diameter / 2;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedalV2.pedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedalV2.pedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball4.ballVelY *= -1;
			} else {
				ball4.ballVelX *= -1;
			}
			ball4.move();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) { // check if press left arrow
			// move left
			if (ball4.ballVelY != 0) {
				pedal.moveLeft();
			}
		} else if (key == KeyEvent.VK_RIGHT) {// check if press right arrow
			// move right
			if (ball4.ballVelY != 0) {
				pedal.moveRight();
			}
		} else if (key == KeyEvent.VK_SPACE) {// check if press spacebar
			// set ballVelY to 4
			// then randomly set ballVelX to 4 or -4
			ball4.ballVelY = 4;
			int random_num = random.nextInt(2);
			if (random_num == 0) {
				ball4.ballVelX = 4;
			} else {
				ball4.ballVelX = -4;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void run() {
		while (true) {
			// bounce off the right or the left of the wall
			if (ball4.x + MyBall.diameter >= super.WIDTH || ball4.x <= 0) {
				ball4.ballVelX *= -1;
			}

			// bouncing off the top and bottom
			if (ball4.y <= 0) {
				ball4.ballVelY *= -1;
			}

			// Check if ball collides with bricks
			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball4, bricks[i][j]);
					}
				}
			}

			// Check if ball hit pedal
			collideWithpedal();

			// Check if ball pass the bottom
			checkPassBottom();

			// move the ball
			ball4.move();
			repaint();

			if (lives == 0 || numVisibleBricks == 0) {
				break;
			}
			
			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {

			}
		}
	}
}
