package suchatpong.nipitphon.lab9;

import javax.swing.SwingUtilities;

public class MyFrameV7 extends MyFrameV6 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV7(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		MyFrameV7 msw = new MyFrameV7("My Frame V7");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV7());
	}
}
