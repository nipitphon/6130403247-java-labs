package suchatpong.nipitphon.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import suchatpong.nipitphon.lab8.MyBall;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MyBallV2 ball2;

	public MyCanvasV6() {
		ball2 = new MyBallV2(WIDTH / 2 - MyBall.diameter / 2, HEIGHT / 2 - MyBall.diameter / 2);
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running = new Thread(this);
		running.start();
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.black);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		g2d.setColor(Color.white);
		g2d.fill(ball2);
	}

	@Override
	public void run() {
		while (true) {
			// bounce the ball when hit the right or the left side of the wall
			if (ball2.x + MyBall.diameter >= super.WIDTH || ball2.x <= 0) {
				ball2.ballVelX *= -1;
			}

			// bouncing the ball when it hits the top and bottom of the wall
			if (ball2.y + MyBall.diameter >= super.HEIGHT || ball2.y <= 0) {
				ball2.ballVelY *= -1;
			}

			// move the ball
			ball2.move();
			repaint();

			// Delay
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}
}
