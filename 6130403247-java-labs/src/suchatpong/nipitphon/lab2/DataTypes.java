package suchatpong.nipitphon.lab2;

public class DataTypes {
	public static void main(String[] args) {
		String name = "Nipitphon suchatpong";
		String ID = "6130403247";
		char FirstLetter = name.charAt(0);
		boolean boooooolean = true;
		int octalID = 57;
		int hexadecimalID = 216;
		long longLastTwoID = 47;
		float floatID = 47.61f;
		double doubleID = 47.61d;

		System.out.println("My name is " + name);
		System.out.println("My student ID is " + ID);
		System.out.println(FirstLetter + " " + boooooolean + " " + octalID + " " + hexadecimalID);
		System.out.println(longLastTwoID + " " + floatID + " " + doubleID);
	}
}

/**
 *Author: Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: January 21, 2019
 **/