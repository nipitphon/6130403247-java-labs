package suchatpong.nipitphon.lab2;

public class StringAPI {
	public static void main(String[] args) {
		boolean test_university = args[0].toLowerCase().contains("university");
		boolean test_college = args[0].toLowerCase().contains("college");

		if (test_university == true) {
			System.out.println(args[0] + " is a University");
		} else if (test_college == true) {
			System.out.println(args[0] + " is a College");
		} else {
			System.out.println(args[0] + " is neither a university nor a college");
		}
	}
}

/**
 *input 1 str 
 *check input if "university" in input will make test_university to true 
 *Likewise college
 *if test_university = true Will show input + " is a University"
 *Likewise college
 * Author: Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: January 21, 2019
 **/