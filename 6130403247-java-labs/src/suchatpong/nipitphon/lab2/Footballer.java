package suchatpong.nipitphon.lab2;

public class Footballer {
	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println("Usage : Footballer <footballer name> <nationality> <club name>");
			System.exit(0);
		}
		String footballer_name = args[0];
		String nationality = args[1];
		String football_club = args[2];
		System.out.println("My favorite football player is " + footballer_name);
		System.out.println("His nationality is " + nationality);
		System.out.println("He plays for " + football_club);
	}
}

/**
 * This Java program that accepts three arguments: your favorite football
 * player, the football club that the player plays for, and the nationality of
 * that player. The output of the program is in the format "My favorite football
 * player is " + <footballer name> "His nationality is " + <nationality> "He
 * plays for " + <football club>
 *
 * Author: Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: January 21, 2019
 **/