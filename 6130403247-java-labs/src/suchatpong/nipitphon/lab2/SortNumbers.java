package suchatpong.nipitphon.lab2;

public class SortNumbers {
	public static void main(String[] args) {
		if (args.length != 5) {
			System.err.println("Usage : number <number1> <number2> <number3> <number4> <number5>");
			System.exit(0);
		}
		double number1 = Double.parseDouble(args[0]);
		double number2 = Double.parseDouble(args[1]);
		double number3 = Double.parseDouble(args[2]);
		double number4 = Double.parseDouble(args[3]);
		double number5 = Double.parseDouble(args[4]);

		double[] array = { number1, number2, number3, number4, number5 };
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (array[i] < array[j]) {
					double temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}
			}
		}
		System.out.println(array[0] + " " + array[1] + " " + array[2] + " " + array[3] + " " + array[4]);
	}
}

/**
 *Input 5 number afte that input 5 number to number1 ,number2 ,number3 ,number4 ,number5 respectively .Then input number to array
 *In loop if array[0] > array[1] Will switch array[0] and array[1] Keep going until the end
 *Next show array Every number
 * Author: Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: January 21, 2019
 **/