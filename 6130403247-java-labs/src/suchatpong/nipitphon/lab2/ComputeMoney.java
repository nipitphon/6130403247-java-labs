package suchatpong.nipitphon.lab2;

public class ComputeMoney {
	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("ComputeMoney : <1000 bath> <500 bath> <100 bath> <20 bath>");
			System.exit(0);
		}

		int sum1000 = 1000 * Integer.valueOf(args[0]);
		int sum500 = 500 * Integer.valueOf(args[1]);
		int sum100 = 100 * Integer.valueOf(args[2]);
		int sum20 = 20 * Integer.valueOf(args[3]);
		float total = sum1000 + sum500 + sum100 + sum20;

		System.out.println("Total Money is " + total);
	}
}

/**
 * Input 4 number by 1st number to 1000 bath ,2nd number tol 500 bath ,3rd number to 100 bath and 4th to 20 bath 
 * Then sum1000 to sum of 1000 bath,sum500 to sum of 500 bath ,sum100 to sum of 100 bath and sum20 to sum of 20 bath
 * Then total to sum of sum1000 ,sum500 ,sum100 and sum20 .Next show total  
 * Author: Nipitphon suchatpong ID: 613040324-7 Sec: 2 Date: January 21, 2019
 **/