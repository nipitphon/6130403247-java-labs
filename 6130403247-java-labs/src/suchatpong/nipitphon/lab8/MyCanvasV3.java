package suchatpong.nipitphon.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MyBall arrayball[] = new MyBall[4];
	protected MyBrick arraybrick[] = new MyBrick[10];
	
	@Override 
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setStroke(new BasicStroke(5));
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);
		g2d.setColor(Color.white);
		
		arrayball[0] = new MyBall(0, 0);
		arrayball[1] = new MyBall(WIDTH - MyBall.diameter, 0);
		arrayball[2] = new MyBall(0, HEIGHT - MyBall.diameter);
		arrayball[3] = new MyBall(WIDTH - MyBall.diameter, HEIGHT - MyBall.diameter);
		
		for (int i = 0 ; i < 10 ; i++) {
			arraybrick[i] = new MyBrick(MyBrick.brickWidth * i, (HEIGHT-MyBrick.brickHeight) / 2);
		}
		
		for (int i = 0 ; i < 4 ; i++) {
			g2d.fill(arrayball[i]);
		}
		
		for (int i = 0 ; i < 10 ; i++) {
			g2d.fill(arraybrick[i]);
			g2d.setColor(Color.black);
			g2d.draw(arraybrick[i]);
			g2d.setColor(Color.white);
		}
	}
}
