package suchatpong.nipitphon.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	
	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setBackground(Color.black);
		setVisible(true);
	}
	
	@Override 
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.white);
		g2d.drawOval(250, 150, 300, 300);
		g2d.fillOval(350, 235, 30, 60);
		g2d.fillOval(425, 235, 30, 60);
		g2d.fillRect(355, 350, 100, 10);
	}
}
