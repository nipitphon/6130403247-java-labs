package suchatpong.nipitphon.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV4(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame4");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV4());
	}
}
