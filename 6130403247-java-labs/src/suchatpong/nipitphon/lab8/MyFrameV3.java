package suchatpong.nipitphon.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyFrameV3(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My FrameV3");
		msw.addComponents();
		msw.setFrameFeatures();
	}
	
	protected void addComponents() {
		add(new MyCanvasV3());
	}
	
	
	
	
}
