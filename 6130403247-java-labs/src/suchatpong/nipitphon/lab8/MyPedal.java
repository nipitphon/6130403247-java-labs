package suchatpong.nipitphon.lab8;

import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static int pedalWidth = 100 ;
	public final static int pedalHeight  = 10 ;
	
	public MyPedal(int x ,int y) {
		super(x ,y ,pedalWidth ,pedalHeight);
	}
}
