package suchatpong.nipitphon.lab8;

import java.awt.geom.Rectangle2D;

public class MyBrick extends Rectangle2D.Double{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static int brickWidth = 80;
	public final static int brickHeight = 20;
	
	public MyBrick (int x ,int y) {
		super(x ,y ,brickWidth ,brickHeight);
	}
}
