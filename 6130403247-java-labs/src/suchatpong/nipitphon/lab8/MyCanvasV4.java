package suchatpong.nipitphon.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected MyBrick myBricks[][] = new  MyBrick[7][10];

	Color colors[] = {Color.magenta,Color.blue,Color.cyan,Color.green,Color.yellow,Color.orange,Color.red};
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);

		for (int i = 0 ; i < 7 ; i++ ) {
			for (int j = 0 ; j < 10; j++) {
				myBricks[i][j] = new MyBrick(MyBrick.brickWidth * j ,HEIGHT/3 - (MyBrick.brickHeight * i));
			}
		}
		
		for (int i = 0 ; i < 7 ; i++ ) {
			for (int j = 0 ; j < 10; j++) {
				g2d.setColor(colors[i]);
				g2d.fill(myBricks[i][j]);
				g2d.setColor(Color.black);
				g2d.draw(myBricks[i][j]);
			}
		}
		
		g2d.setColor(Color.gray);
		g2d.fill(new MyPedal((WIDTH - MyPedal.pedalWidth)/2, HEIGHT - MyPedal.pedalHeight));
		
		g2d.setColor(Color.white);
		g2d.fill(new MyBall((WIDTH - MyBall.diameter)/2 , HEIGHT - MyPedal.pedalHeight - MyBall.diameter));
	}
}
