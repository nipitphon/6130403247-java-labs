package suchatpong.nipitphon.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV11 extends MobileDeviceFormV10 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int MIN_WEIGHT = 100 , MAX_WEIGHT = 3000 ;
	
	public MobileDeviceFormV11(String Topic) {
		super(Topic);
	}

	public void actionPerformed(ActionEvent event) {
		
		Object src = event.getSource();
		menu_action(src);
		
		try {	
			if (src == ok_button) {
				if (text_Model.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please enter model name");
				} else if (Integer.parseInt(text_Weight.getText()) < MIN_WEIGHT) {
					JOptionPane.showMessageDialog(null, "Too light: valid weight is [" + MIN_WEIGHT + "," +  MAX_WEIGHT + "]");
				} else if (Integer.parseInt(text_Weight.getText()) > MAX_WEIGHT) {
					JOptionPane.showMessageDialog(null, "Too heavy: valid weight is [" + MIN_WEIGHT + "," +  MAX_WEIGHT + "]");
				} else if (Integer.parseInt(text_Price.getText()) < 0) { } 
				  else {
					super.actionPerformed(event);
				}
			}
			else if (src == menuitem_display) {
				displaymobiledevice();
			} 
			else if (src == menuitem_sort) {
				sortmobiledevice();
			} 
			else if (src == menuitem_search) {
				searchmobiledevice();
			}
			else if (src == menuitem_remove) {
				removemobiledevice();
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Please enter only numeric input for weight and Price");
		}
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV11 MobileDeviceFormV11 = new MobileDeviceFormV11("Mobile Device Form V11");
		MobileDeviceFormV11.addComponents();
		MobileDeviceFormV11.addMenus();
		MobileDeviceFormV11.setFrameFeatures();
		MobileDeviceFormV11.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
