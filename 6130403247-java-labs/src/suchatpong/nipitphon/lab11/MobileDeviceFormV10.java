package suchatpong.nipitphon.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import suchatpong.nipitphon.lab10.MobileDeviceFormV9;
import suchatpong.nipitphon.lab5.MobileDevice;

public class MobileDeviceFormV10 extends MobileDeviceFormV9 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ArrayList<MobileDevice> arraymobile = new ArrayList<MobileDevice>();
	protected JMenu datamenu;
	protected JMenuItem menuitem_display, menuitem_sort, menuitem_search, menuitem_remove;

	public MobileDeviceFormV10(String Topic) {
		super(Topic);
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();

		if (src == ok_button) {
			if (Android.isSelected()) {
				arraymobile.add(new MobileDevice(text_Model.getText(), "Android",
						Integer.parseInt(text_Price.getText()), Integer.parseInt(text_Weight.getText())));
			} else if (ios.isSelected()) {
				arraymobile.add(new MobileDevice(text_Model.getText(), "ios", Integer.parseInt(text_Price.getText()),
						Integer.parseInt(text_Weight.getText())));
			}

			System.out.println(arraymobile);
		}

		else if (src == menuitem_display) {
			displaymobiledevice();
		} else if (src == menuitem_sort) {
			sortmobiledevice();
		} else if (src == menuitem_search) {
			searchmobiledevice();
		} else if (src == menuitem_remove) {
			removemobiledevice();
		}
	}

	protected void displaymobiledevice() {
		String display_message = "";
		for (int i = 0; i < arraymobile.size(); i++) {
			display_message += Integer.toString(i + 1) + ": " + arraymobile.get(i) + "\n";
		}
		JOptionPane.showMessageDialog(null, display_message);
	}

	protected void sortmobiledevice() {
		Collections.sort(arraymobile, new PriceComparator());
		displaymobiledevice();
	}

	protected void searchmobiledevice() {
		String name_search = JOptionPane.showInputDialog("Please input model name to search: ");
		for (int i = 0; i < arraymobile.size(); i++) {
			if (arraymobile.get(i).getModelName().equals(name_search)) {
				JOptionPane.showMessageDialog(null, arraymobile.get(i) + " is found");
				break;
			} else if (i == arraymobile.size() - 1) {
				JOptionPane.showMessageDialog(null, name_search + " is NOT found");
			}
		}
	}

	protected void removemobiledevice() {
		String name_search = JOptionPane.showInputDialog("Please input model name to search: ");
		for (int i = 0; i < arraymobile.size(); i++) {
			if (arraymobile.get(i).getModelName().equals(name_search)) {
				JOptionPane.showMessageDialog(null, arraymobile.get(i) + " is remove");
				arraymobile.remove(i);
				break;
			} else if (i == arraymobile.size() - 1) {
				JOptionPane.showMessageDialog(null, name_search + " is NOT found");
			}
		}
	}

	protected void updateMenu() {
		datamenu = new JMenu("Data");
		menubar.add(datamenu);

		menuitem_display = new JMenuItem("Display");
		datamenu.add(menuitem_display);

		menuitem_sort = new JMenuItem("Sort");
		datamenu.add(menuitem_sort);

		menuitem_search = new JMenuItem("Search");
		datamenu.add(menuitem_search);

		menuitem_remove = new JMenuItem("Remove");
		datamenu.add(menuitem_remove);
	}

	protected void addMenus() {
		super.addMenus();
		updateMenu();
	}

	protected void addListeners() {
		super.addListeners();
		menuitem_display.addActionListener(this);
		menuitem_sort.addActionListener(this);
		menuitem_search.addActionListener(this);
		menuitem_remove.addActionListener(this);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV10 MobileDeviceFormV10 = new MobileDeviceFormV10("Mobile Device Form V10");
		MobileDeviceFormV10.addComponents();
		MobileDeviceFormV10.addMenus();
		MobileDeviceFormV10.setFrameFeatures();
		MobileDeviceFormV10.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

class PriceComparator implements Comparator<MobileDevice> {
	public int compare(MobileDevice a, MobileDevice b) {
		return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
	}
}
