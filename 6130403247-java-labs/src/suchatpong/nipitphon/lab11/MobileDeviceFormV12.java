package suchatpong.nipitphon.lab11;

import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import suchatpong.nipitphon.lab5.MobileDevice;

public class MobileDeviceFormV12 extends MobileDeviceFormV11 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV12(String Topic) {
		super(Topic);
	}

	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		try {
			if (src == menuitem_save) {
				if (returnval == JFileChooser.APPROVE_OPTION) {
					FileOutputStream fos = new FileOutputStream(file.toString());
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(arraymobile);
					fos.close();
					oos.close();
				}
			} 
			else if (src == menuitem_open) {
				if (returnval == JFileChooser.APPROVE_OPTION) {
					FileInputStream fis = new FileInputStream(file.toString());
					ObjectInputStream ois = new ObjectInputStream(fis);
					arraymobile = (ArrayList<MobileDevice>) ois.readObject();
					System.out.println(arraymobile);
					displaymobiledevice();
					fis.close();
					ois.close();
				}
			}
		} catch (IOException ex) {

		} catch (Exception ex) {

		}
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV12 MobileDeviceFormV12 = new MobileDeviceFormV12("Mobile Device Form V12");
		MobileDeviceFormV12.addComponents();
		MobileDeviceFormV12.addMenus();
		MobileDeviceFormV12.setFrameFeatures();
		MobileDeviceFormV12.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
