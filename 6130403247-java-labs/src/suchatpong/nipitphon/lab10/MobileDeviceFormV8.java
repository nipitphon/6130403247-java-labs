package suchatpong.nipitphon.lab10;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected JMenuItem menuitem_Custom;

	public MobileDeviceFormV8(String Topic) {
		super(Topic);
	}

	protected void updateMenuIcon() {
		super.updateMenuIcon();
		filemenu.setMnemonic(KeyEvent.VK_F);
		
		menuitem_new.setMnemonic(KeyEvent.VK_N);
		menuitem_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_open.setMnemonic(KeyEvent.VK_O);
		menuitem_open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_save.setMnemonic(KeyEvent.VK_S);
		menuitem_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_exit.setMnemonic(KeyEvent.VK_X);
		menuitem_exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
	}
	
	protected void addSubMenus() {
		super.addSubMenus();
		menuitem_Custom = new JMenuItem("Custom �");
		menu_color.add(menuitem_Custom);
		
		configmenu.setMnemonic(KeyEvent.VK_C);
		
		menu_color.setMnemonic(KeyEvent.VK_L);
		
		menuitem_Blue.setMnemonic(KeyEvent.VK_B);
		menuitem_Blue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_green.setMnemonic(KeyEvent.VK_G);
		menuitem_green.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_red.setMnemonic(KeyEvent.VK_R);
		menuitem_red.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
		
		menuitem_Custom.setMnemonic(KeyEvent.VK_U);
		menuitem_Custom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U ,Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
	}
	
	
	public static void createAndShowGUI() {
		MobileDeviceFormV8 MobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		MobileDeviceFormV8.addComponents();
		MobileDeviceFormV8.addMenus();
		MobileDeviceFormV8.setFrameFeatures();
		MobileDeviceFormV8.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
