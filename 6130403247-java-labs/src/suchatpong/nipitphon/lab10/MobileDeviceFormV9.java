package suchatpong.nipitphon.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV9  extends MobileDeviceFormV8 implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected JFileChooser filechooser ;
	protected JColorChooser colorchoose ;
	protected Object srcs ;
	protected int returnval ;
	protected File file ;
	
	
	public MobileDeviceFormV9(String Topic) {
		super(Topic);
	}

	protected void addListeners() {
		super.addListeners();
		menuitem_open.addActionListener(this);
		menuitem_save.addActionListener(this);
		menuitem_exit.addActionListener(this);
		menuitem_red.addActionListener(this);
		menuitem_green.addActionListener(this);
		menuitem_Blue.addActionListener(this);
		menuitem_Custom.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		menu_action(src);
	}
	
	public void menu_action(Object src) {
		filechooser = new JFileChooser();

		if (src == menuitem_open) {
			returnval = filechooser.showOpenDialog(MobileDeviceFormV9.this);
			
			if (returnval == JFileChooser.APPROVE_OPTION) {
				file = filechooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Opening file " + file.getName());
			}
			else {
				JOptionPane.showMessageDialog(null, "Open command cancelled by user") ;
			}	
		}
		else if (src == menuitem_save) {
			returnval = filechooser.showSaveDialog(MobileDeviceFormV9.this);
			
			if (returnval == JFileChooser.APPROVE_OPTION) {
				file = filechooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Saving file " + file.getName());
			}
			else {
				JOptionPane.showMessageDialog(null, "Save command cancelled by user") ;
			}	
		}
		else if (src == menuitem_exit) {
			System.exit(DO_NOTHING_ON_CLOSE);
		}
		
		else if (src == menuitem_green) {
			Review.setBackground(Color.green);
		}
		else if (src == menuitem_red) {
			Review.setBackground(Color.red);
		}
		else if (src == menuitem_Blue) {
			Review.setBackground(Color.blue);
		}
		else if (src == menuitem_Custom) {
			Color color = JColorChooser.showDialog(MobileDeviceFormV9.this, "Choose Color", Review.getBackground());
			Review.setBackground(color); 
		}
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV9 MobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
		MobileDeviceFormV9.addComponents();
		MobileDeviceFormV9.addMenus();
		MobileDeviceFormV9.setFrameFeatures();
		MobileDeviceFormV9.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
