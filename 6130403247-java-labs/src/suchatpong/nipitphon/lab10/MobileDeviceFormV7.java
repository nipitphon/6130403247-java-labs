package suchatpong.nipitphon.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import suchatpong.nipitphon.lab7.MobileDeviceFormV6;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener, ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileDeviceFormV7(String Topic) {
		super(Topic);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();

		if (src == ok_button) {
			handleOKButton();
		} else if (src == cancel_button) {
			handleCancelButton();
		}
	}

	private void handleOKButton() {
		if (Android.isSelected()) {
			JOptionPane.showMessageDialog(null,
							"Brand Name: " + text_Brand.getText() + ", Modal Name: " + text_Model.getText() + ", Weight: "
							+ text_Weight.getText() + ", Price: " + text_Price.getText() + "\nOS: " + Android.getText()
							+ "\nType: " + Type.getSelectedItem() + "\nFeatures: " + jlist.getSelectedValuesList()
							+ "\nReview: " + Review.getText());
		}
		if (ios.isSelected()) {
			JOptionPane.showMessageDialog(null,
							"Brand Name: " + text_Brand.getText() + ", Modal Name: " + text_Model.getText() + ", Weight: "
							+ text_Weight.getText() + ", Price: " + text_Price.getText() + "\nOS: " + ios.getText()
							+ "\nType: " + Type.getSelectedItem() + "\nFeatures: " + jlist.getSelectedValuesList()
							+ "\nReview: " + Review.getText());
		}
	}

	private void handleCancelButton() {
		text_Brand.setText(null);
		text_Model.setText(null);
		text_Weight.setText(null);
		text_Price.setText(null);
		Review.setText(null);
	}

	protected void addComponents() {
		super.addComponents();

		jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Android.setSelected(true);
		jlist.setSelectedIndices(new int[] { 0, 1, 3 });
	}

	protected void addListeners() {
		ok_button.addActionListener(this);
		cancel_button.addActionListener(this);
		Type.addItemListener(this);
		Android.addItemListener(this);
		ios.addItemListener(this);
		jlist.addListSelectionListener(this);
	}

	@Override
	public void itemStateChanged(ItemEvent event) {
		if (event.getSource() == Android || event.getSource() == ios) {
			if (Android.isSelected()) {
				JOptionPane.showMessageDialog(null, "Your os platform is now changed to Android");
			} 
			else if (ios.isSelected()) {
				JOptionPane.showMessageDialog(null, "Your os platform is now changed to ios");
			}
		}
		else if (event.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(null, "Type is updated to " + Type.getSelectedItem());
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent event) {
		if (jlist.getValueIsAdjusting()) {
			JOptionPane.showMessageDialog(null, jlist.getSelectedValuesList());
		}
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceFormV7.addComponents();
		MobileDeviceFormV7.addMenus();
		MobileDeviceFormV7.setFrameFeatures();
		MobileDeviceFormV7.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
